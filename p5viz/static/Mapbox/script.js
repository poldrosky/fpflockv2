// API key for Mapbox. Get one here: https://www.mapbox.com/studio/account/tokens/
var mapbox_key = 'pk.eyJ1IjoiZGllZ29nbGFnYXNoIiwiYSI6ImNrMXV1MWplcDBteXAzY284MzJ2dTNtNWkifQ.mSKI7bXpOKkrKiBY8uTOnA'

// Create an instance of Mapbox
var mappa = new Mappa('Mapbox', mapbox_key);

// var gkey = 'AIzaSyBT5uKP_60rXBYPm3uC9IyCexlpCHnyfNs'
// var mappa = new Mappa('Google', gkey);

// Options for map
var options = {
  //lat: -34.5600311115,
  //lng: -58.4578064548,
  lat: 39.92, // Beijing Lat
  lng: 116.37, // Beijing Lon
  //lat: 50.95, // Tapas lat
  //lng: 6.94, // Tapas lon
  zoom: 10.5,
  width: 800,
  height: 600,
  scale: 1,
  pitch: 0,
}

// Create the static map reference.
var myMap = mappa.staticMap(options);

var img;
var points;

function preload(){
    // Load the image
    img = loadImage(myMap.imgUrl);
    // Load the data
    //points = loadTable('../../data/Tapas_4326_pruning.csv', 'tsv', 'header');
    //flocks = loadTable('../../data/flocks_tapas_4326_20_10_5.csv', 'tsv', 'header');
    points = loadTable('../../data/beijing_alternative_4326.csv', 'tsv', 'header');
    flocks = loadTable('../../data/flocks_beijin_alternative_4326_100_3_7.csv', 'tsv', 'header');
}

//let minlen = 360; // Tapas
//let maxlen = 490; // Tapas
let minlen = 0; // beijing
let maxlen = 1439; // beigjing
let tick = minlen;
let slider;
let checkbox, checkFlock, checkPoints;
let canvas;

function setup(){
    canvas = createCanvas(800,600);
    //canvas.mouseClicked(function() { alert( mouseX + ':' + mouseY ) });
    noStroke();

    frameRate(2);
	
    slider = createSlider(minlen, maxlen, 0);
    slider.position(10, 10);
    //slider.style('width', '200px');
   
    checkbox = createCheckbox('Auto', true);
    checkbox.position(10,70);

    checkPoints = createCheckbox('Points', true);
    checkPoints.position(10,90);

    checkFlock = createCheckbox('Flock', false);
    checkFlock.position(10,110);

    image(img, 0, 0);
} 

function draw()
{
    clear();
	background(102);
	image(img, 0, 0);	
    
	if( checkbox.checked() ) {
		tick++;
		if( tick >= maxlen ) tick = minlen;
		slider.value(tick)
	} else {
		tick = slider.value();
	}	
	
	let c = color(0, 0, 0);
	fill(c);
	textSize(28);
	text(tick, 10, 50);

    if( checkPoints.checked() ) {		
	var rows = points.findRows(tick+'', 'time');
 	for (var i = 0; i < rows.length ; i++) {
		let pos = myMap.latLngToPixel(rows[i].getString('latitude'), rows[i].getString('longitude'));
                let c = color(255, 0, 0);
	        fill(c);
		ellipse(pos.x, pos.y , 3, 3);
	}
    }

    if( checkFlock.checked() ) {
		rows = flocks.findRows(tick+'', 'time');
		for (var i = 0; i < rows.length ; i++) {
			let pos = myMap.latLngToPixel(rows[i].getString('latitude'), rows[i].getString('longitude'));
			let c = color(0, 0, 255);
			let tam = rows[i].getString('members') * 1.1;
			fill(c);
			ellipse(pos.x, pos.y , tam, tam);
		}
	}
}
