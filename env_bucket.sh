#!/bin/bash

if [[ ! -d  "$HOME/bucket_flocks" ]]; then
    mkdir $HOME/bucket_flocks
    gcsfuse bucket_flocks $HOME/bucket_flocks
elif [[ -d "$HOME/bucket_flocks"  && -z "$(mount | grep $HOME/bucket_flocks)" ]]; then
    gcsfuse bucket_flocks $HOME/bucket_flocks
fi


export DATASETS="$HOME/bucket_flocks/Datasets/"
export TEMP="/dev/shm/"
