pycallgraph -ng --include "FPFlock*.*" --include "LCM*.*" --include "main" --include "<module>" --include "getTransactions" --include "flocks" graphviz --output-file=profileFPFlockOnline.png -- ./fpFlockOnline.py --epsilon 500 --mu 3 --delta 3 --file Oldenburg.csv --tag fpFlockOnline

pycallgraph -ng --include "FPFlock*.*" --include "LCM*.*" --include "main" --include "<module>" --include "getTransactions" --include "flocks" graphviz --output-file=profileFPFlockOffline.png -- ./fpFlockOffline.py --epsilon 500 --mu 3 --delta 3 --file Oldenburg.csv --tag fpFlockOffline

pycallgraph graphviz --output-file=prueba.png -- ./fpFlockOnlineV2.py --epsilon 500 --mu 3 --delta 3 --file Oldenburg.csv --tag fpFlockOffline
