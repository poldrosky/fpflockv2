cd ../Src
python mainMaximal.py --epsilon 200 --mu 3 --file Oldenburg.csv
python mainLCM_Maximal.py --epsilon 200 --mu 3 --file Oldenburg.csv
python mainLCM_MaximalV2.py --epsilon 200 --mu 3 --file Oldenburg.csv
python mainLCM_MaximalV2P.py --epsilon 200 --mu 3 --file Oldenburg.csv

python mainMaximal.py --epsilon 300 --mu 9 --file SJ50K55.csv
python mainLCM_Maximal.py --epsilon 300 --mu 9 --file SJ50K55.csv
python mainLCM_MaximalV2.py --epsilon 300 --mu 9 --file SJ50K55.csv
python mainLCM_MaximalV2P.py --epsilon 300 --mu 9 --file SJ50K55.csv

python mainMaximal.py --epsilon 700 --mu 9 --file SJ50K55.csv
python mainLCM_Maximal.py --epsilon 700 --mu 9 --file SJ50K55.csv
python mainLCM_MaximalV2.py --epsilon 700 --mu 9 --file SJ50K55.csv
python mainLCM_MaximalV2P.py --epsilon 700 --mu 9 --file SJ50K55.csv

python mainMaximal.py --epsilon 250 --mu 3 --file beijing_original.csv
python mainLCM_Maximal.py --epsilon 150 --mu 3 --file beijing_original.csv
python mainLCM_MaximalV2.py --epsilon 150 --mu 3 --file beijing_original.csv
python mainLCM_MaximalV2P.py --epsilon 150 --mu 3 --file beijing_original.csv

python mainMaximal.py --epsilon 80 --mu 5 --file beijing_alternative.csv
python mainLCM_Maximal.py --epsilon 80 --mu 5 --file beijing_alternative.csv
python mainLCM_MaximalV2.py --epsilon 80 --mu 5 --file beijing_alternative.csv
python mainLCM_MaximalV2P.py --epsilon 80 --mu 5 --file beijing_alternative.csv
