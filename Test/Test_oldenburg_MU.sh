cd ../Src
printf 'dataset\tepsilon\tmu\tdelta\ttime\tflocks\ttag\n'
python bfe.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag bfetest1
python bfe.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag bfetest1
python bfe.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag bfetest1
python bfe.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag bfetest1
python lcmFlock.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag lcmFlocktest1
python lcmFlock.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag lcmFlocktest1
python lcmFlock.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag lcmFlocktest1
python lcmFlock.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag lcmFlocktest1
python fpFlockOffline.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag fpFlockOfflinetest1
python fpFlockOffline.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag fpFlockOfflinetest1
python fpFlockOffline.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag fpFlockOfflinetest1
python fpFlockOffline.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag fpFlockOfflinetest1
python fpFlockOnline.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag fpFlockOnlinetest1
python fpFlockOnline.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag fpFlockOnlinetest1
python fpFlockOnline.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag fpFlockOnlinetest1
python fpFlockOnline.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag fpFlockOnlinetest1
python mainFPFV2_Offline.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2test1
python mainFPFV2_Offline.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2test1
python mainFPFV2_Offline.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2test1
python mainFPFV2_Offline.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2test1
python mainFPFV2_Online.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2test1
python mainFPFV2_Online.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2test1
python mainFPFV2_Online.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2test1
python mainFPFV2_Online.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2test1
python mainFPFV2P_Offline.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2Ptest1
python mainFPFV2P_Offline.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2Ptest1
python mainFPFV2P_Offline.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2Ptest1
python mainFPFV2P_Offline.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag fpFlockOfflineV2Ptest1
python mainFPFV2P_Online.py --epsilon 300 --mu 5 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2Ptest1
python mainFPFV2P_Online.py --epsilon 300 --mu 7 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2Ptest1
python mainFPFV2P_Online.py --epsilon 300 --mu 9 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2Ptest1
python mainFPFV2P_Online.py --epsilon 300 --mu 11 --delta 3 --file Oldenburg.csv --tag fpFlockOnlineV2Ptest1
