#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  fpFlockOfflineV2.py
#
#  Copyright 2019 Omar Ernesto Cabrera Rosero <omarcabrera@udenar.edu.co>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import LCM_MaximalV2 as lcmMaximal
import time
#import csv
import os
#import Pdbc
#import io
#import sys
#import subprocess
import numpy as np
import argparse

DIRECTORY_DATASETS = os.environ['DATASETS']
DIRECTORY_TEMP = os.environ['TEMP']


class FPFlockOffline(object):
    """ This class is intanced with EPSILON, MU and DELTA"""

    def __init__(self, EPSILON, MU, DELTA):
        self.EPSILON = EPSILON
        self.MU = MU
        self.DELTA = DELTA

    def getTransactions(maximalDisks):
        for maximal in maximalDisks:
            for member in maximalDisks[maximal].members:
                if member not in traj.keys():
                    traj[member] = []
                    traj[member].append(maximalDisks[maximal].id)
                else:
                    traj[member].append(maximalDisks[maximal].id)
        return traj

    def flocks(output1, totalMaximalDisks, keyFlock):
        lines = output1.split('\n')
        lines = lines[1:len(lines)]
        #lines = output1.readlines()
        for line in lines:
            array = list(map(int, line.split(' ')[:-1]))
            if len(array) < DELTA:
                continue
            array.sort()
            members = totalMaximalDisks[array[0]].members
            begin = totalMaximalDisks[array[0]].timestamp
            end = begin
            for element in range(1, len(array)):
                now = totalMaximalDisks[array[element]].timestamp
                if(now == end + 1 or now == end):
                    if(now == end + 1):
                        members = members.intersection(
                            totalMaximalDisks[array[element]].members)
                    end = now

                elif end - begin >= DELTA - 1:
                    b = sorted(members)
                    stdin.append(
                        '{0}\t{1}\t{2}\t{3}'.format(
                            keyFlock, begin, end, b))
                    keyFlock += 1
                    begin = end = now

                else:
                    begin = end = now

            if end - begin >= DELTA - 1:
                b = sorted(members)
                stdin.append(
                    '{0}\t{1}\t{2}\t{3}'.format(
                        keyFlock, begin, end, b))
                keyFlock += 1

        return stdin

    def flockFinder(self, filename, tag):
        global traj
        global stdin
        global DELTA

        lcmMaximal.EPSILON = self.EPSILON
        lcmMaximal.MU = self.MU
        DELTA = self.DELTA

        dataset = np.genfromtxt(
            DIRECTORY_DATASETS + filename,
            delimiter='\t',
            skip_header=1)

        timestamps = sorted(np.unique(dataset[:, 1]))
        
        t1 = time.time()

        keyFlock = 1
        diskID = 1
        traj = {}
        totalMaximalDisks = {}
        stdin = []

        for timestamp in timestamps:
            points = lcmMaximal.pointTimestamp(
                dataset[dataset[:, 1] == timestamp])
            disks = lcmMaximal.disksTimestamp(points, timestamp)
            if not disks:
                continue
            maximalDisks, diskID = lcmMaximal.maximalDisksTimestamp(
                timestamp, diskID)
            totalMaximalDisks.update(maximalDisks)
            FPFlockOffline.getTransactions(maximalDisks)

        st = [t for t in traj.values() if len(t) >= DELTA]
        st = '\n'.join(map(str, st))
        
        output = open('{0}output.dat'.format(DIRECTORY_TEMP), 'w')
        output.write(st)
        output.close()

        cmd_flocks = "./lcm _Cf {0}output.dat {1} -".format(
            DIRECTORY_TEMP, lcmMaximal.MU)
        output1 = os.popen(cmd_flocks).read()
        #output1 = subprocess.getoutput(cmd_flocks)

        stdin = FPFlockOffline.flocks(output1, totalMaximalDisks, keyFlock)

        #table = 'flocks_fpflockofflinev2'
        #print("table: ", table)
        #print("Flocks: ", len(stdin))
        flocks = len(stdin)
        stdin = '\n'.join(stdin)
        #db = Pdbc.DBConnector()
        # db.createTableFlock(table)
        # db.resetTable(table.format(filename))
        # db.copyToTable(table,io.StringIO(stdin))

        t2 = round(time.time() - t1, 3)
        #print("\nTime: ", t2)

        # db.createTableTest()
        print('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}'.format(filename, self.EPSILON, self.MU, DELTA, t2, flocks, tag))
        #db.insertTest(filename,self.EPSILON,self.MU, DELTA, t2, flocks, tag)


def main():
    parser = argparse.ArgumentParser(
            description='Find frequent flock patterns')
    parser.add_argument('--epsilon', help='epsilon')
    parser.add_argument('--mu', help='mu')
    parser.add_argument('--delta', help='delta')
    parser.add_argument('--file', help='name file')
    parser.add_argument('--tag', help='tag')
    
    args = parser.parse_args()
    
    if (
        args.delta==None and
        args.epsilon==None and
        args.file==None and
        args.mu==None and
        args.tag==None):
        
        fp = FPFlockOffline(200, 3, 3)
        fp.flockFinder('Oldenburg.csv', 'fpflockofflineV2Test1')
        #fp = FPFlock(700,9,3)
        #fp.flockFinder('SJ50K55.csv','fpflockofflinev2test5')
    else:
        fp = FPFlockOffline(float(args.epsilon), int(args.mu), int(args.delta))
        fp.flockFinder(str(args.file), str(args.tag))

if __name__ == '__main__':
    main()
