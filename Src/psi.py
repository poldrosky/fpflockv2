import time
import os
import numpy as np
import argparse

DIRECTORY_DATASETS = os.environ['DATASETS']

def PSI(filename, tag, epsilon, mu, delta):
    t1 = time.time()
    name_df = filename.split('.')[0]
    dataset = np.genfromtxt(
                    DIRECTORY_DATASETS + filename,
                    delimiter='\t',
                    skip_header=1)
    dataset[np.lexsort((dataset[:,2], dataset[:,1]))][:, [0,2,3,1]]
    np.savetxt(DIRECTORY_DATASETS+'{0}_psi.txt'.format(name_df),
               dataset[np.lexsort((dataset[:,2], dataset[:,1]))][:, [0,2,3,1]], delimiter='\t',
              fmt=['%d','%f', '%f', '%d'])
    
    stream = os.popen('./build/flocks -m PSI -d {epsilon} -s {mu} -l {delta} -f {DIRECTORY_DATASETS}/{name_df}_psi.txt'\
                      .format(epsilon=epsilon, mu=mu, delta=delta, DIRECTORY_DATASETS=DIRECTORY_DATASETS, name_df=name_df))
    output = stream.read()
    totalTime = float(output.split('\n')[3].split('\t')[1])
    flocks = int(output.split('\n')[3].split('\t')[-1])
    t2 = round(time.time()-t1,3)
    print('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}'.format(filename, epsilon, mu, delta, t2, flocks, tag))


def main():
    parser = argparse.ArgumentParser(
            description='Find frequent flock patterns')
    parser.add_argument('--epsilon', help='epsilon')
    parser.add_argument('--mu', help='mu')
    parser.add_argument('--delta', help='delta')
    parser.add_argument('--file', help='name file')
    parser.add_argument('--tag', help='tag')
    
    args = parser.parse_args()
    
    if (
        args.delta==None and
        args.epsilon==None and
        args.file==None and
        args.mu==None and
        args.tag==None):
              
        PSI('Oldenburg.csv', 'PSITest1', 200, 3, 3)
    else:
        PSI(str(args.file), str(args.tag), float(args.epsilon), int(args.mu), int(args.delta))
        

if __name__ == '__main__':
    main()
