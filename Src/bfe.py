#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  bfe.py
#  
#  Copyright 2014 Omar Ernesto Cabrera Rosero <omarcabrera@udenar.edu.co>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import numpy as np
#import scipy.spatial as ss
#import math
import time
import copy
#import Pdbc
import Maximal
#import io
#import sys
import os
import argparse

DIRECTORY_DATASETS = os.environ['DATASETS']


class Flock(Maximal.Disk):
    def __init__(self, disk, timestamp):
        self.disk = disk
        self.id = self.disk.id
        self.center = self.disk.center
        self.members  = self.disk.members
        self.diskFlock = [self.disk.id]
        self.start = timestamp
        self.end = timestamp

    def getDuration(self):
        return (self.end - self.start) + 1

    def __str__(self):
        return "%s %s" % (self.disk.id, self.disk.members)    

class BFEFlock(object):
    """ This class is intanced with epsilon, mu and delta"""
    def __init__(self, EPSILON, MU, DELTA):
        self.EPSILON = EPSILON
        self.MU = MU
        self.DELTA = DELTA
        
    def flocks(maximalDisks, previousFlocks, timestamp, keyFlock, stdin):
        """Receive maximal disks, previous flocks, tiemstamp, key Flock 
        return previos flock and write filename with flocks """
        currentFlocks = []
        for md in maximalDisks:
            for f in previousFlocks:
                inter = len(maximalDisks[md].members.intersection(f.members))
                if(inter >= Maximal.MU):
                    f1 = copy.copy(f)
                    f1.members = maximalDisks[md].members.intersection(f1.members)
                    f1.diskFlock.append(maximalDisks[md].id)
                    f1.end = timestamp

                    if f1.getDuration() == DELTA:
                        b = list(f1.members)
                        b.sort()
                        stdin.append('{0}\t{1}\t{2}\t{3}'.format(keyFlock, f1.start, f1.end, b))
                        keyFlock += 1
                        f1.start = timestamp - DELTA + 1
                        
                    if f1.start > timestamp  - DELTA:
                        currentFlocks.append(f1)
                                            
            currentFlocks.append(Flock(maximalDisks[md],timestamp))
                    
        previousFlocks = currentFlocks
        return (previousFlocks, keyFlock, stdin)
    
    
    def flockFinder(self, filename,tag):
        global DELTA
        
            
        Maximal.EPSILON = self.EPSILON
        Maximal.MU = self.MU
        DELTA = self.DELTA
        #Maximal.precision = 0.001
                
        dataset = np.genfromtxt(
                DIRECTORY_DATASETS + filename,
                delimiter='\t',
                skip_header=1)

        timestamps = sorted(np.unique(dataset[:, 1]))
        
        t1 = time.time()
        
        previousFlocks = []
        keyFlock = 1
        diskID = 1
        stdin = []
        
        for timestamp in timestamps:
            points = Maximal.pointTimestamp(dataset[dataset[:, 1] == timestamp])
            centersDiskCompare, treeCenters, disksTime = Maximal.disksTimestamp(points, timestamp)    
            if centersDiskCompare == 0:
                continue
            #print(timestamp, len(centersDiskCompare))
            maximalDisks, diskID = Maximal.maximalDisksTimestamp(centersDiskCompare, treeCenters,disksTime, timestamp, diskID)
            #print("Maximal",len(maximalDisks))
            previousFlocks, keyFlock, stdin = BFEFlock.flocks(maximalDisks, previousFlocks, int(timestamp), keyFlock, stdin)
        
        #table = 'flocks_bfe'
        #print("table: ",table)
        #print("Flocks: ",len(stdin))
        flocks = len(stdin)
        stdin = '\n'.join(stdin)
        #db = Pdbc.DBConnector()
        #db.createTableFlock(table)
        #db.resetTable(table.format(filename))
        #db.copyToTable(table,io.StringIO(stdin))

        t2 = round(time.time()-t1,3)
        #print("\nTime: ",t2)
        
        #db.createTableTest()
        print('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}'.format(filename, self.EPSILON, self.MU, DELTA, t2, flocks, tag))
        #db.insertTest(filename,self.epsilon,mu, DELTA, t2, flocks, tag)
        
def main():    
    parser = argparse.ArgumentParser(
            description='Find frequent flock patterns')
    parser.add_argument('--epsilon', help='epsilon')
    parser.add_argument('--mu', help='mu')
    parser.add_argument('--delta', help='delta')
    parser.add_argument('--file', help='name file')
    parser.add_argument('--tag', help='tag')
    
    args = parser.parse_args()
    
    if (
        args.delta==None and
        args.epsilon==None and
        args.file==None and
        args.mu==None and
        args.tag==None):
        
        bfe = BFEFlock(200, 3, 3)
        bfe.flockFinder('Oldenburg.csv', 'bfeTest1')
        #fp = FPFlock(700,9,3)
        #fp.flockFinder('SJ50K55.csv','fpflockofflinev2test5')
    else:
        bfe = BFEFlock(float(args.epsilon), int(args.mu), int(args.delta))
        bfe.flockFinder(str(args.file), str(args.tag))
    

if __name__ == '__main__':
    main()
