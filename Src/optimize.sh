rm *.c*
rm *.so
cythonize --inplace LCM_MaximalV2.py
cythonize --inplace LCM_MaximalV2P.py
cythonize --inplace fpFlockOnlineV2.py
cythonize --inplace fpFlockOfflineV2.py
cythonize --inplace fpFlockOnlineV2P.py
cythonize --inplace fpFlockOfflineV2P.py
