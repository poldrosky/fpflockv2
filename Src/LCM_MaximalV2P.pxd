cimport cython
#cimport numpy as np

cdef class Index:
    cdef public int x, y

cdef class Point:
    cdef public int id, time
    cdef public double x, y
    cdef public object index
    
cdef class Grid:
    cdef public dict dictPoint
    cpdef getPoints(self, Index indexGrid)
    
    @cython.locals(
            index=Index,
            a=cython.int,
            b=cython.int,
            args=cython.tuple)
    cpdef getFrame(self, Point point)
    
cdef class Disk:
    cdef public int id, timestamp
    cdef public set members
    

@cython.locals(
        r2=cython.double,
        X=cython.double, 
        Y=cython.double,
        D2=cython.double,
        expression=cython.double,
        root=cython.double,
        h_1=cython.double,
        k_1=cython.double
        )    
cdef calculateDisks(Point p1, Point p2)

#cdef np.float32_t* dataset 
#cdef np.ndarray[np.float32_t, ndim=2] dataset = np.empty(N, dtype=np.float32)

#cdef pointTimestamp(dataset)

#cdef diskTransactions(Point point)

#cdef filter_points(Point point)
    
#cdef disksTimestamp(object points, int timestamp)
    
#cdef maximalDisksTimestamp(int timestamp, int diskID)





        
