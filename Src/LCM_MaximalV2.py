#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  LCMmaximalV2.py
#
#  Copyright 2019 Omar Ernesto Cabrera Rosero <omarcabrera@udenar.edu.co>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#import csv
import scipy.spatial as ss
#import math
import time
import os
import numpy as np
import argparse


DIRECTORY_DATASETS = os.environ['DATASETS']
DIRECTORY_TEMP = os.environ['TEMP']
PRECISION = 0.001


class Index(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "%s %s" % (self.x, self.y)


class Point(object):
    def __init__(self, *args):
        if len(args) == 4:
            self.id = int(args[0])
            self.time = int(args[1])
            self.x = float(args[2])
            self.y = float(args[3])
            self.index = Index(int(self.x / EPSILON), int(self.y / EPSILON))

        elif len(args) == 2:
            self.x = float(args[0])
            self.y = float(args[1])
        else:
            print('There are problem with this point')

    def __str__(self):
        return "%s %s" % (self.x, self.y)


class Grid(object):
    def __init__(self, dictPoint):
        self.dictPoint = dictPoint

    def getPoints(self, indexGrid):
        try:
            return self.dictPoint[str(indexGrid)]
        except BaseException:
            return []

    def getFrame(self, point):
        index = point.index
        a = index.x
        b = index.y
        args = (
            Grid.getPoints(self, Index(a, b)),
            Grid.getPoints(self, Index(a - 1, b + 1)),
            Grid.getPoints(self, Index(a, b + 1)),
            Grid.getPoints(self, Index(a + 1, b + 1)),
            Grid.getPoints(self, Index(a - 1, b)),
            Grid.getPoints(self, Index(a + 1, b)),
            Grid.getPoints(self, Index(a - 1, b - 1)),
            Grid.getPoints(self, Index(a, b - 1)),
            Grid.getPoints(self, Index(a + 1, b - 1))
        )
        points = np.concatenate(args)

        if (points.size >= MU):
            return points
        else:
            return np.array([])


class Disk(object):
    def __init__(self, id, timestamp, members):
        self.id = id
        self.members = members
        self.timestamp = int(timestamp)


def calculateDisks(p1, p2):
    """
    Calculate the center of the disk passing through two points
    """
    r2 = (EPSILON / 2)**2

    X = p1.x - p2.x
    Y = p1.y - p2.y
    D2 = X**2 + Y**2

    if (D2 == 0):
        return (Point(p1.x, p1.y))

    expression = abs(4 * (r2 / D2) - 1)
    root = expression**0.5
    h_1 = ((X + Y * root) / 2) + p2.x
    #h_2 = ((X - Y * root) / 2) + p2_x
    k_1 = ((Y - X * root) / 2) + p2.y
    #k_2 = ((Y + X * root) / 2) + p2_y

    return (Point(h_1, k_1))
    # return Point(h_2, k_2)


def pointTimestamp(dataset):
    """
    Receive dataset and return objects of points per timestamp
    """
    return [*map(lambda i: Point(i[0], i[1], i[2], i[3]), dataset)]


def diskTransactions(point):
    """
    Receives point by frame and gets the disk if if the number of
    members is greater than MU
    """
    
    pointsFrame = grid.getFrame(point)

    if (pointsFrame.size < MU):
        return ''

    frame = [(i.x, i.y) for i in pointsFrame]

    treeFrame = ss.cKDTree(frame)
    pointsNearestFrame = treeFrame.query_ball_point(
        [point.x, point.y], EPSILON + PRECISION)

    if(len(pointsNearestFrame) < MU):
        return ''

    disks = ''
    for i in pointsNearestFrame:
        p2 = pointsFrame[i]
        if (point.id == p2.id):
            continue

        centerDisk = calculateDisks(point, p2)

        nearestCenter = treeFrame.query_ball_point(
            [centerDisk.x, centerDisk.y], (EPSILON / 2) + PRECISION)

        members = []
        for k in nearestCenter:
            members.append(pointsFrame[k].id)

        if len(members) < MU:
            continue

        disks += (str(members) + '\n')

    return disks


def disksTimestamp(points, timestamp):
    """
    Receive points per timestamp and return center disks compare,
    nearest tree centers and disks per timestamp with yours members
    """
    
    global grid

    dictPoint = {}

    for point in points:
        index = str(point.index)
        if str(index) in dictPoint:
            value = dictPoint[index]
            value.append(point)
        else:
            value = []
            value.append(point)
            dictPoint[index] = value

    grid = Grid(dictPoint)

    disks = [*map(lambda point: diskTransactions(point), points)]

    disks = ''.join(map(str, disks))

    if disks != '':
        output = open(DIRECTORY_TEMP + 'outputDisk.dat', 'w')
        output.write(disks)
        output.close()
        return True
    else:
        return False


def maximalDisksTimestamp(timestamp, diskID):
    """
    This method return the maximal disks per timestamp with LCM
    """

    maximalDisks = {}
    maximalDisks[timestamp] = {}

    cmd_disks = "./lcm _M {0}outputDisk.dat 1 -".format(DIRECTORY_TEMP)
    output1 = os.popen(cmd_disks).read()
    #output1 = subprocess.getoutput(cmd_disks)

    lines = output1.split('\n')
    lines.pop()

    for line in lines:
        array = list(line.split(' '))
        maximalDisks[timestamp][diskID] = Disk(diskID, timestamp, set(array))
        diskID += 1

    maximalDisks = maximalDisks[timestamp]

    return (maximalDisks, diskID)


def maximal(filename, epsilon, mu):
    """This method return maximal disks"""
    global EPSILON
    global MU
    
    EPSILON = epsilon
    MU = mu

    dataset = np.genfromtxt(
        DIRECTORY_DATASETS + filename,
        delimiter='\t',
        skip_header=1)

    t1 = time.time()

    timestamps = sorted(np.unique(dataset[:, 1]))

    diskID = 1

    for timestamp in timestamps:
        points = pointTimestamp(dataset[np.where(dataset[: ,1] == timestamp)])
        disks = disksTimestamp(points, timestamp)
        if not disks:
            continue
        maximalDisks, diskID = maximalDisksTimestamp(timestamp, diskID)
        #print("Maximal", len(maximalDisks))

    t2 = time.time() - t1
    print(filename,EPSILON,MU, t2, 'LCM_MAXIMALV2')
    

def main():
    parser = argparse.ArgumentParser(
            description='Find maximal disks')
    parser.add_argument('--epsilon', help='EPSILON')
    parser.add_argument('--mu', help='MU')
    parser.add_argument('--file', help='name file')
    
    args = parser.parse_args()
    
            
    if (
        args.epsilon==None and
        args.file==None and
        args.mu==None):
                
        maximal('Oldenburg.csv', 200, 3)
        #maximal('SJ50K55.csv', 300, 9)        
    else:
        maximal(str(args.file), float(args.epsilon), int(args.mu))


if __name__ == '__main__':
    main()
