Cython==0.29.13
matplotlib==3.1.0
numpy==1.17.0
pandas==0.25.0
psycopg2-binary==2.8.3
scipy==1.3.1
seaborn==0.9.0
swifter==0.292
